package main

import (
	"log"
	"net/http"
	"strconv"
	_ "net/http/pprof" 
	"net/rpc"

	"entrytask/usersvr/config"
	"entrytask/usersvr/service"
	"entrytask/usersvr/dao"
)
const (
	TCP_CONFIG_PATH = "/Users/wuchao.yan/Desktop/gopath/src/entrytask/usersvr/config/config.conf"
)

func main() {
	config := config.NewConfig()
	err := config.LoadConfig(TCP_CONFIG_PATH)
	if err != nil {
		log.Println("Config Load Err:", err)
		return
	}
	err = dao.InitMysql(config)
	if err != nil {
		log.Println("Mysql Config Load Err:", err)
		return 
	}
	err = dao.InitRedis(config)
	if err != nil {
		log.Println("Redis Config Load Err:", err)
		return 
	}
	rpc.Register(new(service.UserService))
	rpc.HandleHTTP()
	if err != nil {
		log.Println("Init Mysql Or Redis Err:", err)
		return
	}
	go func(){log.Println(http.ListenAndServe("localhost:6060", nil))}() 
	//加个循环？
	err = http.ListenAndServe(":"+strconv.Itoa(config.RpcPort), nil)
	if err != nil {
		log.Fatal("serve error:", err)
	}
}
