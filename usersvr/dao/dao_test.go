package dao

import (
    "testing"

    "entrytask/usersvr/config"
)

func Init(t *testing.T) {
    config := config.NewConfig()
    err := config.LoadConfig("/Users/wuchao.yan/Desktop/gopath/src/entrytask/usersvr/config/config.conf")
    InitMysql(config)
    if err != nil {
        t.Log(err)
        return
    }
    InitRedis(config)
    if err != nil {
        t.Log(err)
        return
    }
}

func Test_QueryUserByName(t *testing.T) {
    Init(t)
    userDAO := new(MysqlDao)
    user, err := userDAO.QueryUserByName("usernam1")
    t.Log(user, err)
}

func Test_QueryUserByID(t *testing.T) {
    Init(t)
    userDAO := new(MysqlDao)
    user, err := userDAO.QueryUserByID(2)
    t.Log(user, err)
}

func Test_UpdateByID(t *testing.T) {
    Init(t)
    userDAO := new(MysqlDao)
    count, err := userDAO.UpdateByID(2, "nickname_update","wqe")
    t.Log(count, err)
}

func Test_Set(t *testing.T) {
    Init(t)
    userDAO := new(RedisDao)
    count, err := userDAO.Set(1)
    t.Log(count, err)
}

func Test_Get(t *testing.T) {
    Init(t)
    userDAO := new(RedisDao)
    count, err := userDAO.Get("first user")
    t.Log(count, err)
}

func Test_Delete(t *testing.T) {
    Init(t)
    userDAO := new(RedisDao)
    err := userDAO.Delete("first user")
    t.Log(err)
}

