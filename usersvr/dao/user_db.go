package dao

import (
	"log"
	"database/sql"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"entrytask/usersvr/config"
	"entrytask/protocol"
)

type MysqlDao struct {
}
var (
	mysqlClient *sql.DB
	mysqlOnce   sync.Once
)


//初始化数据库——Mysql & Redis
func InitMysql(config *config.Config ) error {
	var err error
	//保证初始化一次数据库
	mysqlOnce.Do(func() {
		//连接Mysql数据库
		mysqlClient, err = sql.Open("mysql", config.MysqlName)
		if err != nil {
			return
		}
		err = mysqlClient.Ping()
		if err != nil {
			return
		}
		mysqlClient.SetConnMaxLifetime(time.Minute * time.Duration(config.MysqlConnMaxLifetime))
		mysqlClient.SetMaxIdleConns(config.MysqlMaxIdleConns)
		mysqlClient.SetMaxOpenConns(config.MysqlMaxOpenConns)
	})
	return err
}

func (dao *MysqlDao) QueryUserByName(name string) (*protocol.User, error) {
	user := new(protocol.User)
	row := mysqlClient.QueryRow(
		"SELECT id, name, nickname, password, avatar, IFNULL(salt, '') FROM user_tab WHERE name=?", name)
	err := row.Scan(&user.ID, &user.Name, &user.Nickname, &user.Password, &user.Avatar, &user.Salt)
	return user, err
}

func (dao *MysqlDao) QueryUserByID(id int64) (*protocol.User, error) {
	user := new(protocol.User)
	row := mysqlClient.QueryRow(
		"SELECT id, name, nickname, avatar FROM user_tab WHERE id=?", id)
	err := row.Scan(&user.ID, &user.Name, &user.Nickname, &user.Avatar)
	return user, err
}

func (dao *MysqlDao) UpdateByID(id int64, nickname string, avatar string) (int64, error) {
	geneSql := "UPDATE user_tab SET "
	var sql []interface{}
	if nickname != "" {
		geneSql += "nickname=?,"
		sql = append(sql,nickname)
	}
	if avatar != "" {
		geneSql += "avatar=?,"
		sql = append(sql,avatar)
	}
	geneSql = geneSql[:len(geneSql)-1]
	geneSql += " WHERE id=?"
	sql = append(sql,id)
	res, err := mysqlClient.Exec(geneSql, sql...)
	if err != nil {
		log.Println("update mysql err !!", err)
		return 0, err
	}
	num, err := res.RowsAffected()
	return num, err
}