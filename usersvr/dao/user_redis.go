package dao

import (
	"context"
	"strconv"
	"fmt"
	"time"
	"crypto/md5"
	"math/rand"
	"errors"
	"log"
	"sync"

	"github.com/go-redis/redis"

	"entrytask/usersvr/config"
)

type RedisDao struct{
}

var (
	redisClient *redis.Client
	redisOnce   sync.Once

	NO_USER = errors.New("no user")
)

//初始化Redis
func InitRedis(config *config.Config) error {
	var err error
	//保证初始化一次数据库
	redisOnce.Do(func() {
		//连接Redis
		redisClient = redis.NewClient(&redis.Options{
			Addr:     config.RedisAddr,
			Password: config.RedisPassword,
			DB:       0,
		})
		_, err := redisClient.Ping(redisClient.Context()).Result()
		if err != nil {
			log.Println("err!:", err)
		}
	})
	return err
}

func (dao *RedisDao) Set(ID int64) (string, error) {
	if ID < 0 {
		return "", NO_USER//常量
	} 
	md5Obj := md5.New()
	md5Obj.Write([]byte(fmt.Sprintf("%v:%v:%v", ID, time.Now().Unix(), rand.Int63n(1000))))
	code := fmt.Sprintf("%x", md5Obj.Sum(nil))
	err := redisClient.Set(context.Background(), code, strconv.FormatInt(ID, 10), 10*60*time.Second).Err()//定义成常量
	if err != nil {
		return "", err
	}
	return code, err
}

func (dao *RedisDao) Get(key string) (int64, error) {//get
	//查看缓存
	val, err := redisClient.Get(context.Background(), key).Result()
	if err != nil {
		return 0, err
	}
	id, err := strconv.ParseInt(val, 10, 64)
	if err!= nil{
		return 0, err
	}
	return id, err
}

func (dao *RedisDao) Delete(key string) error {
	err := redisClient.Del(context.Background(), key).Err()
	return err
}
