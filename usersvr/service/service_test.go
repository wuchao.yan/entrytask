package service

import (
    "testing"

    "entrytask/usersvr/config"
    "entrytask/protocol"
    "entrytask/usersvr/dao"
)

func Init(t *testing.T) {
    config := config.NewConfig()
    err := config.LoadConfig("/Users/wuchao.yan/Desktop/gopath/src/entrytask/usersvr/config/config.conf")
    dao.InitMysql(config)
    if err != nil {
        t.Log(err)
        return
    }
    dao.InitRedis(config)
    if err != nil {
        t.Log(err)
        return
    }
}

func Test_Login(t *testing.T) {
    Init(t)
    userService := new(UserService)
    objReq := new(protocol.LoginReq)
    objResp := new(protocol.LoginResp)
    objReq.Name = "user1"
    objReq.Password = "user1"
    err := userService.Login(objReq, objResp)
    t.Log(err)
    t.Log(objResp)
}


func Test_Auth(t *testing.T) {
    Init(t)
    userService := new(UserService)
    objReq := new(protocol.AuthReq)
    objResp := new(protocol.AuthResp)
    objReq.Code = "user12e"
    err := userService.Auth(objReq, objResp)
    t.Log(err)
    t.Log(objResp)
}

func Test_Logout(t *testing.T) {
    Init(t)
    userService := new(UserService)
    objReq := new(protocol.LogoutReq)
    objResp := new(protocol.LogoutResp)
    objReq.Code = "user12e"
    err := userService.Logout(objReq, objResp)
    t.Log(err)
    t.Log(objResp)
}

func Test_Query(t *testing.T) {
    Init(t)
    userService := new(UserService)
    objReq := new(protocol.QueryReq)
    objResp := new(protocol.QueryResp)
    objReq.ID = 1
    objReq.Code = "user12e"
    err := userService.Query(objReq, objResp)
    t.Log(err)
    t.Log(objResp)
}

func Test_Update(t *testing.T) {
    Init(t)
    userService := new(UserService)
    objReq := new(protocol.UpdateReq)
    objResp := new(protocol.UpdateResp)
    objReq.User.Name = "user1"
	objReq.User.Password = "password1"
	objReq.User.Salt = "cwew"
	objReq.User.Nickname = "user1"
	objReq.User.ID = 1
	objReq.User.Avatar = "eqdq"
    objReq.Code = "user12e"
    err := userService.Update(objReq, objResp)
    t.Log(err)
    t.Log(objResp)
}
