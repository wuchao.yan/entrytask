package service

import (
	"database/sql"
	"crypto/sha1"
	"fmt"
	"log"
	"errors"

	_ "github.com/go-sql-driver/mysql"

	"entrytask/usersvr/dao"
	"entrytask/protocol"
)
const(
	CORRECT       	int32 = 0
	NOUSER        	int32 = 1000
	PASSWORD_ERROR  int32 = 1001
	FAILED_CASH     int32 = 1002
	FAILED_AUTH  	int32 = 1003
	FAIL_DEL_CASH 	int32 = 1004
	QUERY_ERROR   	int32 = 1005
)

var(
	ERR_NOUSER         = errors.New("No User!")
	ERR_PASSWORD_ERROR = errors.New("Password Error!")
)

type UserService struct {
	mysqlDao dao.MysqlDao
	redisDao dao.RedisDao
}

//登陆
func (t *UserService) Login(objReq *protocol.LoginReq, objResp *protocol.LoginResp) error {
	//获取用户的信息，利用姓用户名查询
	user, err := t.mysqlDao.QueryUserByName(objReq.Name)
	if err != nil {
		if err == sql.ErrNoRows {
			objResp.ErrorCode = NOUSER //用户不存在
		}
		return err
	}
	shaObj := sha1.New()
    shaObj.Write([]byte(objReq.Password + user.Salt))
    reqPassword := fmt.Sprintf("%x", shaObj.Sum(nil))
    if reqPassword != user.Password {
        log.Println("targetPassword", reqPassword)
		objResp.ErrorCode = PASSWORD_ERROR
		return ERR_PASSWORD_ERROR
    }
	//登陆成功后将用户id生成Auth
	code, err := t.redisDao.Set(user.ID)
	if err != nil {
		objResp.ErrorCode = FAILED_CASH //缓存失败
	}
	//设置页面response，将加密数据返回
	objResp.Code = code
	objResp.ErrorCode = CORRECT 
	return nil
}

//鉴权
func (t *UserService) Auth(objReq *protocol.AuthReq, objResp *protocol.AuthResp) error {
	id, err := t.redisDao.Get(objReq.Code)
	if err != nil {
		objResp.ErrorCode = FAILED_AUTH //鉴权失败
		return  err
	}
	objResp.ID = id
	objResp.ErrorCode = CORRECT 
	return nil
	
}

//登出
func (t *UserService) Logout(objReq *protocol.LogoutReq, objResp *protocol.LogoutResp) error {
	_, err := t.redisDao.Get(objReq.Code)
	if err != nil {
		objResp.ErrorCode = FAILED_AUTH //鉴权失败
		return  err
	}
	err = t.redisDao.Delete(objReq.Code)
	if err != nil {
		objResp.ErrorCode = FAIL_DEL_CASH  //删除缓存失败
		return err
	}
	objResp.ErrorCode = CORRECT 
	return err
}

//查询用户信息
func (t *UserService) Query(objReq *protocol.QueryReq, objResp *protocol.QueryResp) error {
	user, err := t.mysqlDao.QueryUserByID(objReq.ID)
	if err != nil {
		if err == sql.ErrNoRows {
			objResp.ErrorCode = NOUSER //用户不存在
		}
		return err
	}
	/*
	if objResp.User == nil{
		objResp.ErrorCode = QUERY_ERROR  //查询异常
	} else {
	*/	
		objResp.User.Name = user.Name
		objResp.User.Password = user.Password
		objResp.User.Salt = user.Salt
		objResp.User.Nickname = user.Nickname
		objResp.User.ID = user.ID
		objResp.User.Avatar = user.Avatar
		objResp.ErrorCode = CORRECT 
	//}
	return nil
}

//更新用户信息
func (t *UserService) Update(objReq *protocol.UpdateReq, objResp *protocol.UpdateResp) error {
	var err error
	objResp.Count, err = t.mysqlDao.UpdateByID(objReq.User.ID, objReq.User.Nickname,objReq.User.Avatar)
	return err
}
