package config

import (  
    "testing"
)


func Test_Config(t *testing.T) {
   config := NewConfig()
   err := config.LoadConfig("config.conf")
   t.Log(err)
   t.Log(config.RpcIP)
   t.Log(config.RpcPort)
   t.Log(config.MysqlMaxIdleConns)
   t.Log(config.MysqlMaxOpenConns)
   t.Log(config.MysqlConnMaxLifetime)
   t.Log(config.MysqlName)
   t.Log(config.RedisAddr)
   t.Log(config.RedisPassword)
}
