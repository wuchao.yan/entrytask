package config

import (
	"bufio"
	"errors"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
)

var (
	config *Config
	once   sync.Once
)

func NewConfig() *Config {
	if config == nil {
		once.Do(func() {
			config = &Config{}
		})
	}

	return config
}

type Config struct {
	RpcIP        		 string
	RpcPort       		 int
	RpcAddr       		 string
	MysqlName     		 string
	RedisAddr     		 string
	RedisPassword 		 string
	MysqlMaxIdleConns 	 int
	MysqlMaxOpenConns  	 int
	MysqlConnMaxLifetime int
}

func (this *Config) LoadConfig(path string) error {
	log.Println("Read Config Path:", path)
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	buf := bufio.NewReader(file)
	for {
		line, err := buf.ReadString('\n')
		if err == io.EOF {
			break
		}
		line = strings.TrimSpace(line)
		if len(line) <= 0 {
			continue
		}
		pair := strings.SplitN(line, "=", 2)
		key := pair[0]
		val := pair[1]
		switch key {
		case "RpcIP":
			this.RpcIP = val
			log.Printf("Set Value %s=%s\n", key, val)
		case "RpcPort":
			this.RpcPort, err = strconv.Atoi(val)
			log.Printf("Set Value %s=%s\n", key, val)
		case "MysqlName":
			this.MysqlName = val
			log.Printf("Set Value %s=%s\n", key, val)
		case "RedisPassword":
			this.RedisPassword = val
			log.Printf("Set Value %s=%s\n", key, val)
		case "MysqlMaxIdleConns":
			this.MysqlMaxIdleConns, err = strconv.Atoi(val)
			log.Printf("Set Value %s=%s\n", key, val)
		case "MysqlMaxOpenConns":
			this.MysqlMaxOpenConns, err = strconv.Atoi(val)
			log.Printf("Set Value %s=%s\n", key, val)
		case "MysqlConnMaxLifetime":
			this.MysqlConnMaxLifetime, err = strconv.Atoi(val)
			log.Printf("Set Value %s=%s\n", key, val)
		}
	}
	if len(this.RpcIP) == 0 || this.RpcPort == 0 {
		return errors.New("Some Config no Found")
	}
	this.RpcAddr = this.RpcIP + ":" + strconv.Itoa(this.RpcPort)
	return nil
}
