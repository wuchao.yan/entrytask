DROP DATABASE IF EXISTS entry_task;
CREATE DATABASE entry_task;

USE DATABAE;

 CREATE TABLE 'user_tab' (
    'id' bigint(20) NOT NULL COMMENT '用户id',
    'name' varchar(40) DEFAULT NULL COMMENT '用户姓名',
    'nickname' varchar(40) DEFAULT NULL COMMENT '用户昵称',
    'avatar' varchar(128) DEFAULT NULL COMMENT '用户头像URL',
    'password' varchar(40) DEFAULT NULL COMMENT '密码',
    'salt' varchar(6) DEFAULT NULL COMMENT '加密盐',
    PRIMARY KEY ('id'),
    UNIQUE KEY 'username' ('name')
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '用户信息'