package main

import (
    "database/sql"
    "time"
    "fmt"
    "math/rand"
    "crypto/sha1"

    _ "github.com/go-sql-driver/mysql"

    "entrytask/tcpsvr/config"
)

const (
	TCP_CONFIG_PATH = "/Users/wuchao.yan/Desktop/gopath/src/entrytask/tcpsvr/config/config.conf"
)
func main() {
    config := config.NewConfig()
    err := config.LoadConfig(TCP_CONFIG_PATH)
    mysqlClient, err := sql.Open("mysql", config.MysqlName)
    if err != nil {
        return
    }
    err = mysqlClient.Ping()
    if err != nil {
        return
    }
    // configure driver
    mysqlClient.SetConnMaxLifetime(time.Minute * 5)
    mysqlClient.SetMaxIdleConns(config.MysqlMaxIdleConns)
    mysqlClient.SetMaxOpenConns(config.MysqlMaxOpenConns)


    for id:=1; id<=10000000 ;id++ {
        avatar := fmt.Sprintf("user%vname", id)
        name := fmt.Sprintf("user%v", id)
        nickname := fmt.Sprintf("nickname%v", id)
        userpassword := fmt.Sprintf("password%v", id)
        salt := fmt.Sprintf("%6x", rand.Intn(10000000))
        shaObj := sha1.New()
        shaObj.Write([]byte(userpassword + salt))
        password := fmt.Sprintf("%x", shaObj.Sum(nil))

        mysqlClient.Exec("INSERT INTO user_tab(id, name, nickname, password, salt, avatar)value(?,?,?,?,?,?)", id, name, nickname, password, salt, avatar)
    }

}
