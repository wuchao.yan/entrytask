package protocol

type User struct {
    ID       int64
    Name     string
    Nickname string
    Avatar   string
    Password string
    Salt     string
}
const(
	SERVICE_USER_CHECK_AUTH = "UserService.Auth"
	SERVICE_USER_LOGIN      = "UserService.Login"
	SERVICE_USER_LOGOUT     = "UserService.Logout"
	SERVICE_USER_QUERY      = "UserService.Query"
	SERVICE_USER_UPDATE     = "UserService.Update"
)

//登陆
type LoginReq struct {
	Name     string
	Password string
}

type LoginResp struct {
	Code string
	ErrorCode int32
}

//鉴权
type AuthReq struct {
	Code string
}

type AuthResp struct {
	ID int64
	ErrorCode int32
}

//退出
type LogoutReq struct {
	Code string
}

type LogoutResp struct {
	ErrorCode int32
}

//获取用户信息
type QueryReq struct {
	ID int64
	Code string
}

type QueryResp struct {
	User      User
	ErrorCode int32
}

//更新用户信息
type UpdateReq struct {
	Code string
	User User
}

type UpdateResp struct {
	Count     int64
	ErrorCode int32
}
