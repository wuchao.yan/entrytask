package service

import (
    "testing"
    "net/http"
    "net/url"
    "bytes"
    "io"
    "io/ioutil"
    "mime/multipart"
    "os"
    "time"
    "fmt"

    "github.com/tidwall/gjson"
)

var (
    Code string
   // my_url = "http://c-dancer.com"
)

func TestLogin(t *testing.T) {
    resp, err := http.PostForm("http://127.0.0.1:8080/login", url.Values{"Name":{"user1"}, "Password":{"password1"}})
    if err != nil {
        t.Log(err)
        fmt.Println(err)
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    t.Log(err)
    t.Log(string(body))
    if err != nil {
        Code = gjson.Get(string(body), "data.Code").String()
        fmt.Println(Code)
    }
}

func TestQuery(t *testing.T) {
    bodyBuf := &bytes.Buffer{}
    objReq, err := http.NewRequest("GET", "http://127.0.0.1:8080/query", bodyBuf)
    objReq.AddCookie(&http.Cookie{
       Name: "Code",
       Value: Code,
       Expires: time.Now().Add(10000*time.Second),
    })

    client := &http.Client{}
    resp, err := client.Do(objReq)
    t.Log(err)
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    t.Log(err)
    t.Log(string(body))
}

func TestUpdate(t * testing.T) {
    bodyBuf := &bytes.Buffer{}
    bodyWriter := multipart.NewWriter(bodyBuf)
    params := map[string]string{
        "Nickname" : "nickName1",
        "Avatar"   : "avatar1",
    }
    for key, val := range params {
        _ = bodyWriter.WriteField(key, val)
    }
    contentType := bodyWriter.FormDataContentType()
    bodyWriter.Close()

    objReq, err := http.NewRequest("POST", "http://127.0.0.1:8080/update", bodyBuf)
    objReq.AddCookie(&http.Cookie{
       Name: "Code",
       Value: Code,
       Expires: time.Now().Add(10000*time.Second),
    })
    objReq.Header.Set("Content-Type", contentType)
    client := &http.Client{}
    resp, err := client.Do(objReq)
    t.Log(err)
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    t.Log(err)
    t.Log(string(body))
}

func TestUpload(t * testing.T) {
    file, err := os.Open("/Users/wuchao.yan/Desktop/gopath/src/entrytask/userapi/static/avatar/user1avatar.jpg")
    if err != nil {
        t.Log("error opening file")
        return
    }
    defer file.Close()

    bodyBuf := &bytes.Buffer{}
    bodyWriter := multipart.NewWriter(bodyBuf)

    filename := "fighting.gif"
    fileWriter, err := bodyWriter.CreateFormFile("file", filename)
    if err != nil {
        t.Log("error writing to buffer")
        return
    }
    _, err = io.Copy(fileWriter, file)
    if err != nil {
        t.Log(err)
        return
    }
    contentType := bodyWriter.FormDataContentType()
    params := map[string]string{
        "filename" : filename,
    }
    for key, val := range params {
        _ = bodyWriter.WriteField(key, val)
    }
    bodyWriter.Close()

    objReq, err := http.NewRequest("PUT", "http://127.0.0.1:8080/upload", bodyBuf)
    objReq.AddCookie(&http.Cookie{
       Name: "Code",
       Value: Code,
       Expires: time.Now().Add(10000*time.Second),
    })
    objReq.Header.Set("Content-Type", contentType)

    client := &http.Client{}
    resp, err := client.Do(objReq)
    t.Log(err)

    body, err := ioutil.ReadAll(resp.Body)
    t.Log(err)
    t.Log(string(body))
}

func TestLogout(t *testing.T) {
    objReq, err := http.NewRequest("DELETE", "http://127.0.0.1:8080/logout", nil)
    objReq.AddCookie(&http.Cookie{
       Name: "Code",
       Value: Code,
       Expires: time.Now().Add(10000*time.Second),
    })
    objReq.Header.Add("Content-Type", "application/json")
    client := &http.Client{}
    resp, err := client.Do(objReq)
    if err != nil {
        t.Log(err)
    }
    //t.Log(err)
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    t.Log(err)
    t.Log(string(body))
}