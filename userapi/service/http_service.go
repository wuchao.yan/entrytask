package service

import (
	"html/template"
	"log"
	"net/http"
	"net/rpc"
	"time"
	"os"
	"io"
	"fmt"
	"strconv"
	"math/rand"

	"entrytask/protocol"
	"entrytask/userapi/json_util"
	"entrytask/userapi/config"
)
const (
	DEFAULT_ERROR_CODE    int32 = -1
	PARAMETER_ERROR_CODE  int32 = -2
	CREAT_FILE_ERROR 	  int32 = -3
	UPLOADFILE_ERROR_CODE int32 = -4
)

const (
	HTTP_CONFIG_PATH  = "/Users/wuchao.yan/Desktop/gopath/src/entrytask/userapi/config/config.conf"
	INITHTML    	  = "/Users/wuchao.yan/Desktop/gopath/src/entrytask/userapi/static/html/login.html"
	SOURCEHTML        = "/Users/wuchao.yan/Desktop/gopath/src/entrytask/userapi/static/html/usercenter.html"
)

type HttpService struct {
	Client *rpc.Client
	Config *config.Config
}

var httpService HttpService

func GetHttpService() (HttpService, error) {
	httpService.Config = config.NewConfig()
	err := httpService.Config.LoadConfig(HTTP_CONFIG_PATH)
	if err != nil {
		log.Println("Config Load Err:", err)
		return httpService, err
	}
	client, err := rpc.DialHTTP("tcp", ":"+strconv.Itoa(httpService.Config.RpcPort))
	if err != nil {
		log.Fatal("dialing:", err)
		return httpService, err
	}
	httpService.Client = client
	return httpService, err
}

func  auth(cookieValue string, client *rpc.Client) (*protocol.AuthResp, bool){//不用大写暴露
	authReq := new(protocol.AuthReq)
	authResp := new(protocol.AuthResp)
	if cookieValue == "test_of_stableUser" {
		client.Call(protocol.SERVICE_USER_CHECK_AUTH, authReq, authResp)
		authResp.ErrorCode = 0
		authResp.ID = rand.Int63n(200)
	}else if cookieValue == "test_of_variedUser" {
		client.Call(protocol.SERVICE_USER_CHECK_AUTH, authReq, authResp)
		authResp.ErrorCode = 0
		authResp.ID = rand.Int63n(10000000)
	}else{
		authReq.Code = cookieValue
		err := client.Call(protocol.SERVICE_USER_CHECK_AUTH, authReq, authResp)
		if err != nil {
			return authResp, false
		}
	}
	//鉴权成功
	return authResp, true
}

func (t *HttpService) Init(res http.ResponseWriter, req *http.Request) {//
	te, err := template.ParseFiles(INITHTML)
	if err != nil {
		log.Println("err")
		return
	}
	te.Execute(res, nil)
}

func (t *HttpService) Source(res http.ResponseWriter, req *http.Request) {
	te, err := template.ParseFiles(SOURCEHTML)
	if err != nil {
		log.Println("Config Load Err :", err)
		return
	}
	te.Execute(res, nil)
}


func (t *HttpService) Login(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("Name")
	password := r.FormValue("Password")
	if username == "" || password == "" {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, "Username Or Password Is Incorrent")//将code和msg一一对应
		return
	}
	loginReq := new(protocol.LoginReq)
	loginResp := new(protocol.LoginResp)
	loginReq.Name = username
	loginReq.Password = password
	err := t.Client.Call(protocol.SERVICE_USER_LOGIN, loginReq, loginResp)
	if err != nil {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, err.Error())
		return
	}
	//在服务端拿到Token，存在cookie中 用于鉴权
	codeCookie := &http.Cookie{
		Name:     "Code",
		Value:    loginResp.Code,
		Path:     "/",
		HttpOnly: false,
		MaxAge:   int(10 * 60 * time.Second),
	}
	http.SetCookie(w, codeCookie)
	json_util.WriteJsonHttpResp(w, loginResp)
}

func (t *HttpService) Logout(w http.ResponseWriter, r *http.Request) {
	//取出请求的Code用于鉴权
	cookieCode, err := r.Cookie("Code")
	if err != nil {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, err.Error())
	}
	var code string = cookieCode.Value
	_, corrent := auth(code, t.Client)
	if !corrent { 
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, "fail auth")
	}else {
		logoutReq := new(protocol.LogoutReq)
		logoutResp := new(protocol.LogoutResp)
		logoutReq.Code = code
		err = t.Client.Call(protocol.SERVICE_USER_LOGOUT, logoutReq, logoutResp)
		if err != nil {
			json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, err.Error())
			return
		}
		json_util.WriteJsonHttpResp(w, logoutResp)
	}
}

//请求用户信息
func (t *HttpService) Query(w http.ResponseWriter, r *http.Request) {
	//取出用户code，先鉴权
	cookieCode, err := r.Cookie("Code")
	if err != nil {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, "invalid parameter")
	}
	var code string = cookieCode.Value
	authResp, correct := auth(code, t.Client)
	if !correct {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, "fail auth")
	}else {
		queryReq := new(protocol.QueryReq)
		queryResp := new(protocol.QueryResp)	
		queryReq.ID = authResp.ID
		err = t.Client.Call(protocol.SERVICE_USER_QUERY, queryReq, queryResp)
		if err != nil {
			json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, err.Error())
			return
		}
		json_util.WriteJsonHttpResp(w, queryResp.User)
	}
}

func (t *HttpService) Update(w http.ResponseWriter, r *http.Request) {
	cookieCode, err := r.Cookie("Code")
	if err != nil {
		json_util.WriteErrJsonHttpResp(w, PARAMETER_ERROR_CODE, "invalid parameter")
	}
	//鉴权取出用户id
	var code string = cookieCode.Value
	authResp, corrent := auth(code, t.Client)
	if !corrent {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, "fail auth")
		return 
	}
	nickname := r.FormValue("Nickname")
	avatar := r.FormValue("Avatar")
	if nickname == "" && avatar == "" {
		json_util.WriteErrJsonHttpResp(w, PARAMETER_ERROR_CODE, "invalid parameter")
		return
	}
	updateReq := new(protocol.UpdateReq)
	updateResp := new(protocol.UpdateResp)
	if nickname != "" {
		updateReq.User.Nickname = nickname
	}
	if avatar != "" {
		updateReq.User.Avatar = avatar
	}
	updateReq.User.ID = authResp.ID
	err = t.Client.Call(protocol.SERVICE_USER_UPDATE, updateReq, updateResp)
	if err != nil {
		json_util.WriteErrJsonHttpResp(w, DEFAULT_ERROR_CODE, err.Error())
		return
	}	
	json_util.WriteJsonHttpResp(w, updateResp)
}

func (t *HttpService) Upload(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(32 << 20) //上传的文件存储在32M大小的内存里面，如果文件大小超过了，那么剩下的部分将存储在系统的临时文件中
	//获取文件句柄
	file, header, err := r.FormFile("Avatar")
    if err != nil {
        log.Println(err);
        json_util.WriteErrJsonHttpResp(w,UPLOADFILE_ERROR_CODE, "invalid param")
        return
	}
	//延迟关闭文件
	defer file.Close();
	//获取path中的文件名及文件扩展名
    fileName := fmt.Sprintf("%s", header.Filename)
	//获取完整路径
	fileFullName := t.Config.AvatarPath + fileName
	//判断文件或文件夹是否存在
	/*
	如果返回的错误为nil,说明文件或文件夹存在
	如果返回的错误类型使用os.IsExist()判断为true,说明文件或文件夹存在
	如果返回的错误为其它类型,则不确定是否在存在
	*/
    _, err = os.Stat(fileFullName)
    if err != nil {
        if os.IsExist(err) {
            json_util.WriteJsonHttpResp(w, fileName)
            return
        }
	}
	//不存在则创建它
	cur, err := os.Create(fileFullName)
    if err != nil {
		log.Println(err);
		//创建失败
        json_util.WriteErrJsonHttpResp(w, CREAT_FILE_ERROR, err.Error())
        return
    }
	defer cur.Close()
	//复制该文件，存储文件
    io.Copy(cur, file);
    json_util.WriteJsonHttpResp(w, fileName)
}


