package json_util

import (
	"encoding/json"
	"log"
	"net/http"
)

func WriteJsonHttpResp(objResp http.ResponseWriter, data interface{}) {
	objResp.Header().Set("Content-type", "application/json")
	respMap := map[string]interface{} { "data": data, "err_msg": "", "err_code":0 }
	respJson, err := json.Marshal(respMap)
	if err != nil {
		log.Println("WriteJson Err:", err)
		return
	}
	objResp.Write(respJson)
}

func WriteErrJsonHttpResp(objResp http.ResponseWriter, code int32, msg string) {
	objResp.Header().Set("Content-type", "application/json")
	respMap := map[string]interface{} { "data": "", "err_msg": msg, "err_code": code }
	respJson, err := json.Marshal(respMap)
	if err != nil {
		log.Println("WriteJson Err:", err)
		return
	}
	objResp.Write(respJson)
}
