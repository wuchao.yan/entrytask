package main

import (
	"log"
	"net/http"

	"entrytask/userapi/service"
)

func main() {

	server, err := service.GetHttpService()
	if err != nil {
		log.Println("err:",err)
		return 
	}
	http.Handle("/userapi/static/", http.FileServer(http.Dir("../")))
	http.HandleFunc("/usercenter", server.Source)
	http.HandleFunc("/login", server.Login)
	http.HandleFunc("/logout", server.Logout)
	http.HandleFunc("/query", server.Query)
	http.HandleFunc("/update", server.Update)
	http.HandleFunc("/upload", server.Upload)
	http.HandleFunc("/", server.Init)

	http.ListenAndServe(server.Config.HttpAddr, nil)
}
