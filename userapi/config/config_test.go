package config

import (  
    "testing"
)


func Test_Config(t *testing.T) {
   config := NewConfig()
   err := config.LoadConfig("config.conf")
   t.Log(err)
   t.Log(config.HttpIP)
   t.Log(config.HttpPort)
   t.Log(config.RpcIP)
   t.Log(config.RpcPort)
}
