package config

import (
	"bufio"
	"errors"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
)

var (
	config *Config
	once sync.Once
)

func NewConfig() *Config {
	if config == nil {
		once.Do(func () {
			config = &Config {}
		})
	}

	return config
}

type Config struct {
	HttpIP     string
	HttpPort   int
	HttpAddr   string
	RpcIP      string
	RpcPort    int
	RpcAddr    string
	StaticPath string
	AvatarPath string
}

func (this *Config) LoadConfig(path string) error {
	log.Println("Read Config Path:", path)
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	buf := bufio.NewReader(file)
	for {
		line, err := buf.ReadString('\n')
		if err  == io.EOF {
			break
		}
		line = strings.TrimSpace(line)
		if len(line) <= 0 {
			continue
		}
		pair := strings.SplitN(line, "=",2)
		key := pair[0]
		val := pair[1]
		switch key {
		case "HttpIP" :
			this.HttpIP = val
			log.Printf("Set Value %s=%s\n", key, val)
		case "HttpPort" :
			this.HttpPort,err = strconv.Atoi(val)
			log.Printf("Set Value %s=%s\n", key, val)
		case "RpcIP":
			this.RpcIP = val
			log.Printf("Set Value %s=%s\n", key, val)
		case "RpcPort":
			this.RpcPort, err = strconv.Atoi(val)
			log.Printf("Set Value %s=%s\n", key, val)
		case "AvatarPath":
			this.AvatarPath = val
            log.Printf("Set Value %s=%s\n", key, val)
		case "StaticPath":
			this.StaticPath = val
			log.Printf("Set Value %s=%s\n", key, val)
		}
	}
	if (len(this.HttpIP) == 0 && len(this.RpcIP) == 0)  || ( this.RpcPort == 0 && this.HttpPort == 0) {
		return errors.New("Some Config no Found")
	}
	this.RpcAddr = this.RpcIP + ":" + strconv.Itoa(this.RpcPort)
	this.HttpAddr = this.HttpIP + ":" + strconv.Itoa(this.HttpPort)
	return nil
}

