function getSearchParam(key) {
    var pairs = location.search.substring(1).split("&")
    for(var i = 0; i < pairs.length; i++) {
        var kv = pairs[i].split("=")
        if(kv[0] === key) {
            return kv[1]
        }
    }
    return ""
}